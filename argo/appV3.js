
// Define protocol header (5 bytes)
const ucProtocol_Header = new Uint8Array([0x23, 0x41, 0x52, 0x47, 0x23]); // "#ARG#" in ASCII

// Define protocol footer (5 bytes)
const ucProtocol_Footer = new Uint8Array([0x23, 0x45, 0x4E, 0x44, 0x23]); // "#END#" in ASCII


// Command index for CMD_DEVICE_CONFIG response
const CMD_OFFSET = 19;                  // Index where the command (0x04fe) is located in the response
const ACTION_OFFSET = 20;               // Action offset for ACK/NACK check

const SIZE_UUID = 14;              // 14 bytes for UUID
const SIZE_DEVNAME = 32;           // Device name size
const NUM_HOURS = 24;              // Number of hours in a day
const MAX_SIZE_APN = 32;           // Maximum size for APN
const MAX_SIZE_SERVER_NAME = 32;   // Maximum size for server name
const MAX_SIZE_USER_PWD = 32;      // Maximum size for user password
const MAX_SIZE_NETWORK_NAME = 16;  // Maximum size for network name
const BUFFER_SIZE = 1024;           // Adjust buffer size to accommodate data
const DATA_OFFSET = 21;            // Starting offset for the data


// CRC-8-CCITT related constants (already implemented in Crc8CCITT function)
const CRC_INITIAL = 0x00;               // Initial CRC value (usually 0x00)


const zDevice = {
	ulMagic: 0, // unsigned long

	ucDevice_Type: 0, // unsigned char
	ucGSM_Enable: 0,  // unsigned char
	ucLoRA_Enable: 0, // unsigned char

	usDevice_CP: 0, // unsigned short
	ucDevice_District: 0, // unsigned char
	usTecnico_Master_ID: 0, // unsigned short
	ucLast_User_Access: 0, // unsigned char

	ucUID: new Uint8Array(SIZE_UUID), // 14 bytes UID
	ucDeviceName: new Uint8Array(SIZE_DEVNAME), // Device Name

	usEvent_Flags: 0, // unsigned short
	usEvent_Mask: 0, // unsigned short
	usEvent_WakeUp: 0, // unsigned short

	ucLock0_Hook_Detector: 0, // unsigned char
	ucLock1_Hook_Detector: 0, // unsigned char

	ucBat_Type: 0, // unsigned char
	usBat_Voltage: 0, // unsigned short
	usBat_DesignCap: 0, // unsigned short
	ucBat_Capatity: 0, // unsigned char
	ucBat_EOL: 0, // unsigned char

	ulNumLogsStored: 0, // unsigned long

	ucReporting_Hours_Cloud: new Uint8Array(NUM_HOURS), // unsigned char array
	ucLock_Hours_Enabled: new Uint8Array(NUM_HOURS), // unsigned char array

	ucDefaultApn_Name: new Uint8Array(MAX_SIZE_APN), // unsigned char array
	ucDefaultUserPass: new Uint8Array(MAX_SIZE_USER_PWD), // unsigned char array
	ucDefaultServerName: new Uint8Array(MAX_SIZE_SERVER_NAME), // unsigned char array
	usDefaultPort_Number: 0, // unsigned short

	ucApn_Name: new Uint8Array(MAX_SIZE_APN), // unsigned char array
	ucUserPass: new Uint8Array(MAX_SIZE_USER_PWD), // unsigned char array
	ucServerName: new Uint8Array(MAX_SIZE_SERVER_NAME), // unsigned char array
	usPort_Number: 0, // unsigned short
	usSIM_PIN: 0, // unsigned short

	ucCobertura: 0, // unsigned char
	ucLastAccTechnology: 0, // unsigned char
	ucLastAccNetwork: new Uint8Array(MAX_SIZE_NETWORK_NAME), // unsigned char array

	ulNum_Cloud_Connections: 0, // unsigned long
	ulNum_Lock_Apertures: 0, // unsigned long

	usLog_Next_Minute: 0, // unsigned short
	usEvent_Mask_Saved: 0, // unsigned short
	usEvent_WakeUp_Saved: 0, // unsigned short

	timeOutBle: 0 // int
};

let firstTime = 0;
let isConnected = false; // Initially not connected
let testCP = 0;
let device, characteristic;
let numMessages = 0;
let configDev;
let deviceData = [];
let getLogs = 0;
let tmpNumLogs = 0;
let logsTotaltoDownload = 0;

let server;

function isiOS() {
    return /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;
}

$(document).ready(function() {
    if (isiOS()) {
        $('#saveLogsButton').text('Copy Logs');
    } else {
        $('#saveLogsButton').text('Save Logs');
    }
});

// To clear all logs
function clearLogs() {
    $('#logsContent').empty();
}

// Function to add a log message
function addLog(message) {
    $('#logsContent').append('<p>' + message + '</p>');
}

// Assuming 'dateTimeDevice' is a string representing the date and time
function updateDeviceDateTime(dateTimeDevice) {
    $('#deviceDateTime').text(dateTimeDevice);
}

// Assuming 'battery' is a number between 0 and 100
function updateBatteryLevel(battery) {
    $('#batteryLevel').text(battery + '%');

    // Remove existing battery level classes
    $('#batteryIcon').removeClass('fa-battery-empty fa-battery-quarter fa-battery-half fa-battery-three-quarters fa-battery-full');

    // Update battery icon based on battery level
    if (battery >= 75) {
        $('#batteryIcon').addClass('fa-battery-full');
    } else if (battery >= 50) {
        $('#batteryIcon').addClass('fa-battery-three-quarters');
    } else if (battery >= 25) {
        $('#batteryIcon').addClass('fa-battery-half');
    } else if (battery >= 10) {
        $('#batteryIcon').addClass('fa-battery-quarter');
    } else {
        $('#batteryIcon').addClass('fa-battery-empty');
    }
}

function updateConnectButton() {
    const connectButton = $('#connect');
    if (isConnected) {
        // Disconnect state
        connectButton
            .removeClass('btn-connect')
            .addClass('btn-disconnect')
            .html('<i class="fa fa-unlink fa-lg" aria-hidden="true"></i> Disconnect');
    } else {
        // Connect state
        connectButton
            .removeClass('btn-disconnect')
            .addClass('btn-connect')
            .html('<i class="fa fa-bluetooth fa-lg" aria-hidden="true"></i> Connect to BLE Device');
    }
}

function updateConnectionStatus(connection) {
    if (connection) {
        // Connected state
        $('#connectionIcon')
            .removeClass('fa-times-circle')
            .addClass('fa-check-circle')
            .attr('title', 'Connected');
    } else {
        // Disconnected state
        $('#connectionIcon')
            .removeClass('fa-check-circle')
            .addClass('fa-times-circle')
            .attr('title', 'Disconnected');
    }
}


function createDeviceConfigPayload() {
	let iSize = 0;

	// Prepare the data array with enough size for header, UUID, payload, footer, and CRC
	const data = new Uint8Array(BUFFER_SIZE);
	//const data = new Uint8Array(5 + SIZE_UUID + 125 + 5 + 1); // Header + UUID + Payload + Footer + CRC

	// Add the protocol header
	data.set(ucProtocol_Header, iSize);
	iSize += ucProtocol_Header.length;

	// Add the UUID
	data.set(zDevice.ucUID, iSize);
	iSize += SIZE_UUID;

	// Add command and access (setConfigDev, CMD = 0x04, ACCESS = 0x01)
	data[iSize] = 0x04; // Command for setConfigDev
	iSize += 1;
	data[iSize] = 0x01; // Access (WR = 1)
	iSize += 1;

	// Add the actual device config data (payload)
	data[iSize] = zDevice.ucDevice_Type;
	iSize += 1;
	data[iSize] = zDevice.ucGSM_Enable;
	iSize += 1;
	data[iSize] = zDevice.ucLoRA_Enable;
	iSize += 1;

	// Add Postal code (usDevice_CP)
	data[iSize] = zDevice.usDevice_CP & 0xFF;
	data[iSize + 1] = (zDevice.usDevice_CP >> 8) & 0xFF;
	iSize += 2;

	data[iSize] = zDevice.ucDevice_District;
	iSize += 1;

	// Add Technical Master ID (usTecnico_Master_ID)
	data[iSize] = zDevice.usTecnico_Master_ID & 0xFF;
	data[iSize + 1] = (zDevice.usTecnico_Master_ID >> 8) & 0xFF;
	iSize += 2;

	// Add device name
	console.log("1");
	//const deviceNameStr = new TextDecoder().decode(zDevice.ucDeviceName);
	//const deviceNameArray = new TextEncoder().encode(deviceNameStr.padEnd(SIZE_DEVNAME, '\0'));
	const deviceNameArray = new TextEncoder().encode(zDevice.ucDeviceName.padEnd(SIZE_DEVNAME, '\0'));
	data.set(deviceNameArray, iSize);
	iSize += SIZE_DEVNAME;

	// Add event flags, mask, and wakeup
	data[iSize] = zDevice.usEvent_Flags & 0xFF;
	data[iSize + 1] = (zDevice.usEvent_Flags >> 8) & 0xFF;
	iSize += 2;

	data[iSize] = zDevice.usEvent_Mask & 0xFF;
	data[iSize + 1] = (zDevice.usEvent_Mask >> 8) & 0xFF;
	iSize += 2;

	data[iSize] = zDevice.usEvent_WakeUp & 0xFF;
	data[iSize + 1] = (zDevice.usEvent_WakeUp >> 8) & 0xFF;
	iSize += 2;

	// Add Lock Detector and battery details
	data[iSize] = zDevice.ucLock0_Hook_Detector;
	iSize += 1;
	data[iSize] = zDevice.ucBat_Type;
	iSize += 1;

	// Add battery design capacity
	data[iSize] = zDevice.usBat_DesignCap & 0xFF;
	data[iSize + 1] = (zDevice.usBat_DesignCap >> 8) & 0xFF;
	iSize += 2;

	// Add APN name
	console.log("2");
	//const apnStr = new TextDecoder().decode(zDevice.ucApn_Name);
	//const apnArray = new TextEncoder().encode(apnStr.padEnd(SIZE_DEVNAME, '\0'));
	const apnArray = new TextEncoder().encode(zDevice.ucApn_Name.padEnd(MAX_SIZE_APN, '\0'));
	data.set(apnArray, iSize);
	iSize += MAX_SIZE_APN;

	// Add user pass
	console.log("3");
	//const userPassStr = new TextDecoder().decode(zDevice.ucUserPass);
	//const userPassArray = new TextEncoder().encode(userPassStr.padEnd(SIZE_DEVNAME, '\0'));
	const userPassArray = new TextEncoder().encode(zDevice.ucUserPass.padEnd(MAX_SIZE_USER_PWD, '\0'));
	data.set(userPassArray, iSize);
	iSize += MAX_SIZE_USER_PWD;

	// Add server name
	console.log("4");
	//const serverNameStr = new TextDecoder().decode(zDevice.ucServerName);
	//const serverNameArray = new TextEncoder().encode(serverNameStr.padEnd(SIZE_DEVNAME, '\0'));
	const serverNameArray = new TextEncoder().encode(zDevice.ucServerName.padEnd(MAX_SIZE_SERVER_NAME, '\0'));
	data.set(serverNameArray, iSize);
	iSize += MAX_SIZE_SERVER_NAME;

	// Add port number and SIM pin
	data[iSize] = zDevice.usPort_Number & 0xFF;
	data[iSize + 1] = (zDevice.usPort_Number >> 8) & 0xFF;
	iSize += 2;
	data[iSize] = zDevice.usSIM_PIN & 0xFF;
	data[iSize + 1] = (zDevice.usSIM_PIN >> 8) & 0xFF;
	iSize += 2;

	// Add remaining device parameters
	//	data[iSize] = zDevice.ucCobertura;
	//	data[iSize] = zDevice.ucLastAccTechnology;

	data[iSize] = zDevice.ulNum_Lock_Apertures & 0xFF;
	data[iSize + 1] = (zDevice.ulNum_Lock_Apertures >> 8) & 0xFF;
	data[iSize + 2] = (zDevice.ulNum_Lock_Apertures >> 16) & 0xFF;
	data[iSize + 3] = (zDevice.ulNum_Lock_Apertures >> 24) & 0xFF;
	iSize += 4;

	data[iSize] = zDevice.ulNum_Cloud_Connections & 0xFF;
	data[iSize + 1] = (zDevice.ulNum_Cloud_Connections >> 8) & 0xFF;
	data[iSize + 2] = (zDevice.ulNum_Cloud_Connections >> 16) & 0xFF;
	data[iSize + 3] = (zDevice.ulNum_Cloud_Connections >> 24) & 0xFF;
	iSize += 4;

	// Add reporting hours and lock hours
	data.set(zDevice.ucReporting_Hours_Cloud, iSize);
	iSize += NUM_HOURS;
	data.set(zDevice.ucLock_Hours_Enabled, iSize);
	iSize += NUM_HOURS;

	// Add protocol footer
	data.set(ucProtocol_Footer, iSize);
	iSize += ucProtocol_Footer.length;

	// Add CRC
	const crc = crc8CCITT(0x00, data.slice(0, iSize),iSize);
	data[iSize] = crc;
	iSize += 1;

	// Log iSize and CRC
	console.log("iSize: ", iSize);
	console.log("CRC: ", crc);

	// Convert to hex string and log
	const hexString = Array.from(data.slice(0, iSize)).map(byte => byte.toString(16).padStart(2, '0')).join('');
	console.log("Hex data before sending:", hexString);

	// Return the data array
	return data.slice(0, iSize);  // Return only the valid portion of the array
}



// Helper function to pad strings to a fixed length (32 bytes, etc.)
function padStringArray(byteArray, maxLength) {
	const paddedArray = new Uint8Array(maxLength);
	paddedArray.set(byteArray);
	return paddedArray;
}


function appendMessage(text) {
	//const messagesDiv = document.getElementById('messages');
	//messagesDiv.innerHTML += text + "<br>";
	//messagesDiv.scrollTop = messagesDiv.scrollHeight; // Scroll to bottom
}

function printDeviceConfig() {
	let output = "------------------- Print Device Config ------------------\n";

	// Device UUID
	output += `Device UUID: ${Array.from(zDevice.ucUID).map(b => b.toString(16).padStart(2, '0')).join('')} \n`;

	// Firmware Version
	output += `FW Version: ${zDevice.ucFW_MAJOR}.${zDevice.ucFW_MINOR} \n`;

	// Device Name
	//output += `Device Name: [${new TextDecoder().decode(zDevice.ucDeviceName)}] \n`;
	// Device Name (already a string, no need to decode)
	output += `Device Name: [${zDevice.ucDeviceName}] \n`;


	// Device Type, Postal Code, District
	output += `Device Type: ${zDevice.ucDevice_Type} \n`;
	output += `Postal Code: ${zDevice.usDevice_CP} \n`;
	output += `Device District: ${zDevice.ucDevice_District} \n`;

	// Technical Master ID and User Count
	output += `Technical Master ID: ${zDevice.usTecnico_Master_ID} \n`;

	// GSM and LoRa Enabling
	output += `GSM Enable: ${zDevice.ucGSM_Enable} \n`;
	output += `LoRa Enable: ${zDevice.ucLoRA_Enable} \n`;

	// Event Flags, Mask, and WakeUp
	output += `Event Flags: 0x${zDevice.usEvent_Flags.toString(16)} \n`;
	output += `Event Mask: 0x${zDevice.usEvent_Mask.toString(16)} \n`;
	output += `Event WakeUp: 0x${zDevice.usEvent_WakeUp.toString(16)} \n`;

	// Lock Detector
	output += `Lock Detector: 0x${zDevice.ucLock0_Hook_Detector.toString(16)} \n`;

	// Battery Information
	//output += `Battery Type: 0x${zDevice.ucBat_Type.toString(16)} \n`;
	output += `Battery Type: 0x${(zDevice.ucBat_Type !== undefined ? zDevice.ucBat_Type.toString(16) : 'undefined')} \n`;
	output += `Battery Voltage: ${zDevice.usBat_Voltage} mV \n`;
	output += `Battery Capacity: ${zDevice.ucBat_Capatity} % \n`;
	output += `Battery Design Capacity: ${zDevice.usBat_DesignCap} mAh \n`;
	output += `Battery EOL: 0x${zDevice.ucBat_EOL.toString(16)} \n`;

	// APN, User Pass, Server Name
	//output += `APN Name: [${new TextDecoder().decode(zDevice.ucApn_Name)}] \n`;
	//output += `User Pass: [${new TextDecoder().decode(zDevice.ucUserPass)}] \n`;
	//output += `Server Name: [${new TextDecoder().decode(zDevice.ucServerName)}] \n`;

	output += `APN Name: [${zDevice.ucApn_Name}] \n`;
	output += `User Pass: [${zDevice.ucUserPass}] \n`;
	output += `Server Name: [${zDevice.ucServerName}] \n`;
	// Port Number, SIM PIN, Cobertura
	output += `Port Number: ${zDevice.usPort_Number} \n`;
	output += `SIM PIN: ${zDevice.usSIM_PIN} \n`;
	output += `Cobertura: ${zDevice.ucCobertura} \n`;

	// Last Access Technology and Network
	output += `Last Access Technology: ${zDevice.ucLastAccTechnology} \n`;
	output += `Last Access Network: [${zDevice.ucLastAccNetwork}] \n`;

	// Logs and Statistics
	output += `Number of Logs Stored: ${zDevice.ulNumLogsStored} \n`;
	output += `Number of Lock Apertures: ${zDevice.ulNum_Lock_Apertures} \n`;
	output += `Number of Cloud Connections: ${zDevice.ulNum_Cloud_Connections} \n`;

	// Cloud Schedule
	output += "Cloud Schedule: ";
	for (let i = 0; i < NUM_HOURS; i++) {
		output += `[${i}]:${zDevice.ucReporting_Hours_Cloud[i]} `;
	}
	output += "\n";

	// Lock Schedule
	output += "Lock Schedule: ";
	for (let i = 0; i < NUM_HOURS; i++) {
		output += `[${i}]:${zDevice.ucLock_Hours_Enabled[i]} `;
	}
	output += "\n";

    console.log(output);
	// Display everything in an alert
	//document.getElementById('messages').innerHTML = `<pre>${output}</pre>`;

}


function parseDeviceConfig(responseArray) {
	let iSize = DATA_OFFSET;

	// Device Type, GSM Enable, LoRA Enable (1 byte each)
	zDevice.ucDevice_Type = responseArray[iSize++];
	zDevice.ucGSM_Enable = responseArray[iSize++];
	zDevice.ucLoRA_Enable = responseArray[iSize++];

	// Firmware Major and Minor Versions (1 byte each)
	zDevice.ucFW_MAJOR = responseArray[iSize++];
	zDevice.ucFW_MINOR = responseArray[iSize++];

	// Postal Code (2 bytes, little-endian)
	zDevice.usDevice_CP = responseArray[iSize++] | (responseArray[iSize++] << 8);

	// Device District (1 byte)
	zDevice.ucDevice_District = responseArray[iSize++];

	// Technical Master ID (2 bytes, little-endian)
	zDevice.usTecnico_Master_ID = responseArray[iSize++] | (responseArray[iSize++] << 8);

	// Device Name (SIZE_DEVNAME bytes), remove padding null bytes
	zDevice.ucDeviceName = new TextDecoder().decode(responseArray.slice(iSize, iSize + SIZE_DEVNAME)).replace(/\0/g, '');
	iSize += SIZE_DEVNAME;

	// Event Flags, Mask, and WakeUp (2 bytes each, little-endian)
	zDevice.usEvent_Flags = responseArray[iSize++] | (responseArray[iSize++] << 8);
	zDevice.usEvent_Mask = responseArray[iSize++] | (responseArray[iSize++] << 8);
	zDevice.usEvent_WakeUp = responseArray[iSize++] | (responseArray[iSize++] << 8);

	// Lock Detector (1 byte)
	zDevice.ucLock0_Hook_Detector = parseInt(responseArray[iSize++]);

	// Battery Info (Type, Voltage, Capacity, Design Capacity)
	zDevice.ucBat_Type = responseArray[iSize++];
	console.log("test  zDevice.ucBat_Type");
	console.log("ucBat_Type "+ zDevice.ucBat_Type);

	zDevice.usBat_Voltage = responseArray[iSize++] | (responseArray[iSize++] << 8);
	zDevice.ucBat_Capatity = responseArray[iSize++];
	zDevice.usBat_DesignCap = responseArray[iSize++] | (responseArray[iSize++] << 8);

    updateBatteryLevel(zDevice.ucBat_Capatity);

	// Battery EOL (1 byte)
	zDevice.ucBat_EOL = responseArray[iSize++];

	// APN Name (MAX_SIZE_APN bytes), remove padding null bytes
	zDevice.ucApn_Name = new TextDecoder().decode(responseArray.slice(iSize, iSize + MAX_SIZE_APN)).replace(/\0/g, '');
	iSize += MAX_SIZE_APN;

	// User Pass (MAX_SIZE_USER_PWD bytes), remove padding null bytes
	zDevice.ucUserPass = new TextDecoder().decode(responseArray.slice(iSize, iSize + MAX_SIZE_USER_PWD)).replace(/\0/g, '');
	iSize += MAX_SIZE_USER_PWD;

	// Server Name (MAX_SIZE_SERVER_NAME bytes), remove padding null bytes
	zDevice.ucServerName = new TextDecoder().decode(responseArray.slice(iSize, iSize + MAX_SIZE_SERVER_NAME)).replace(/\0/g, '');
	iSize += MAX_SIZE_SERVER_NAME;

	// Port Number (2 bytes, little-endian)
	zDevice.usPort_Number = responseArray[iSize++] | (responseArray[iSize++] << 8);

	// SIM PIN (2 bytes, little-endian)
	zDevice.usSIM_PIN = responseArray[iSize++] | (responseArray[iSize++] << 8);

	// Coverage, Last Access Technology, Last Access Network
	zDevice.ucCobertura = responseArray[iSize++];
	zDevice.ucLastAccTechnology = responseArray[iSize++];

	// Last Access Network (MAX_SIZE_NETWORK_NAME bytes), remove padding null bytes
	zDevice.ucLastAccNetwork = new TextDecoder().decode(responseArray.slice(iSize, iSize + MAX_SIZE_NETWORK_NAME)).replace(/\0/g, '');
	iSize += MAX_SIZE_NETWORK_NAME;

	// Logs and Stats (4 bytes each, little-endian)
	zDevice.ulNumLogsStored = responseArray[iSize++] | (responseArray[iSize++] << 8) | (responseArray[iSize++] << 16) | (responseArray[iSize++] << 24);
	zDevice.ulNum_Lock_Apertures = responseArray[iSize++] | (responseArray[iSize++] << 8) | (responseArray[iSize++] << 16) | (responseArray[iSize++] << 24);
	zDevice.ulNum_Cloud_Connections = responseArray[iSize++] | (responseArray[iSize++] << 8) | (responseArray[iSize++] << 16) | (responseArray[iSize++] << 24);

	// Cloud and Lock Schedules (NUM_HOURS bytes each)
	zDevice.ucReporting_Hours_Cloud = responseArray.slice(iSize, iSize + NUM_HOURS);
	iSize += NUM_HOURS;

	zDevice.ucLock_Hours_Enabled = responseArray.slice(iSize, iSize + NUM_HOURS);
	iSize += NUM_HOURS;

	return iSize;
}



// Parse the UUID before calling parseDeviceConfig
function parseUUID(responseArray) {
	const UUID_OFFSET = 5;
	const SIZE_UUID = 14;

	// Parse the 14-byte UUID starting from offset 5
	zDevice.ucUID = responseArray.slice(UUID_OFFSET, UUID_OFFSET + SIZE_UUID);
	console.log("Parsed Device UUID:", zDevice.ucUID);
}

async function enableNotifications() {
	try {
		await characteristic.startNotifications();
		console.log('Notifications started');

		characteristic.addEventListener('characteristicvaluechanged', (event) => {

			const value = event.target.value;
			let hexString = '';
			for (let i = 0; i < value.byteLength; i++) {
				hexString += value.getUint8(i).toString(16).padStart(2, '0');
		}
			console.log('D-Received', hexString);
			let sizePacket = value.byteLength;
			console.log("with size : " + sizePacket);

			if (hexString == '30' )
			{
				//document.getElementById('numMessages').innerHTML = "NUM LOGS : 0";
				clearLogs();
    		    addLog('NUM LOGS : 0');
        		$('#logsModal').modal('show');
			}
			else if ( getLogs == 1 && hexString.startsWith('55'))
			{
                tmpNumLogs++;
                logsTotaltoDownload--;
                const responseArray = new Uint8Array(hexString.match(/.{1,2}/g).map(byte => parseInt(byte, 16)));
                console.log("Num tmp logs : " + responseArray[1]);
                console.log("Message full : " + hexString);
                console.log("Message real and good : " + hexString.slice(4));
                console.log("tmp num logs :" + tmpNumLogs);
                console.log("logsTotaltoDownload :" + logsTotaltoDownload);
				addLog(hexString.slice(4));
				$('#logsModal').modal('show');
				console.log("condition tmp : " + (tmpNumLogs == responseArray[1]));
                if ( tmpNumLogs == responseArray[1] && logsTotaltoDownload > 0)
                {
                    alert("Readed all temporal logs, do clear and read again.");
                    getLogs = 0;
                    //hexString.slice(4);
                }
                if ( logsTotaltoDownload == 0 )
                {
                    alert("All logs downloaded");
                }
			}
			else if (hexString.startsWith('01') || hexString.startsWith('02') || hexString.startsWith('00') || hexString.startsWith('04') )
			{
				//numMessages++;
				//document.getElementById('numMessages').innerHTML = "NUM LOGS : "+numMessages;
				//appendMessage(hexString);
			}
			else if ( hexString.startsWith('504f'))
			{
				if ( hexString.charAt(11) == '1' )
				{
					//document.getElementById('numMessages').innerHTML = "LOCK OPENED";


				}
				else
				{
					//document.getElementById('numMessages').innerHTML = "LOCK CLOSED";
				}
			}
			else if ( hexString.startsWith('2341'))
			{
				let decoder = new TextDecoder('utf-8');
				//appendMessage(decoder.decode(value));
				//appendMessage(hexString);
				configDev = hexString;

				// Convert the hex string back to a Uint8Array
				const responseArray = new Uint8Array(hexString.match(/.{1,2}/g).map(byte => parseInt(byte, 16)));


				// Check if this is a valid getConfigDev response by checking the command (04fe)
				const cmdIndex = CMD_OFFSET; // Adjust this index if necessary based on your protocol
				const expectedCmd = [0x04, 0xfe];

				if (responseArray[cmdIndex] === expectedCmd[0] && responseArray[cmdIndex + 1] === expectedCmd[1] && sizePacket > 50) {
					console.log('Valid getConfigDev response detected');

					// Validate the CRC
					const crcReceived = responseArray[responseArray.length - 1]; // Last byte is CRC
					const calculatedCRC = crc8CCITT(0x00, responseArray, responseArray.length - 1); // Exclude last byte (CRC)

					if (crcReceived === calculatedCRC) {
						console.log('CRC is valid.');

						// Store the result (excluding CRC) in the global array
						deviceData = responseArray.slice(0, responseArray.length - 1);
						console.log('Stored device data:', deviceData);

						// Parse the device config and fill the zDevice structure
						parseUUID(responseArray); // Parse the UUID
						console.log("------------------------- pre parserdeviceconfig");
						parseDeviceConfig(responseArray); // Parse the rest of the data
                        if ( firstTime == 1 )
                        {
                            firstTime = 0;
                            if ( zDevice.ulNumLogsStored > 0 )
                            {
                                alert("There are logs : " + zDevice.ulNumLogsStored);
                                logsTotaltoDownload = zDevice.ulNumLogsStored;
                            }
                        }

						printDeviceConfig();
					} else {
						console.error('CRC is invalid.');
					}
				} else {
					// Optionally show the message as a log if it's not a config response
					//        appendMessage(hexString);
					// Validate the CRC
					const crcReceived = responseArray[responseArray.length - 1]; // Last byte is CRC
					const calculatedCRC = crc8CCITT(0x00, responseArray, responseArray.length - 1); // Exclude last byte (CRC)

					if (crcReceived === calculatedCRC) {
						console.log('CRC is valid.');

						// Store the result (excluding CRC) in the global array
						deviceData = responseArray.slice(0, responseArray.length - 1);
						console.log('Stored device data:', deviceData);

						// Optional: Process the device data as needed
					} else {
						console.error('CRC is invalid.');
					}
					if ( testCP == 1 )
					{
						setTimeout(function() {
							testCP = 0;
							GetConfig(); 
						}, 1000);
					}
				}

			}
			else if ( hexString.startsWith('3078'))
			{
				let decoder = new TextDecoder('utf-8');
				const epochTimeSeconds = parseInt(decoder.decode(value), 16);
				const date = new Date(epochTimeSeconds * 1000);
				console.log(date.toString());
				//alert("Set DATE/TIME  OK - "+date.toString());
 
                // Format the date into 'YYYY-MM-DD HH:MM'
                const year = date.getFullYear();
                const month = String(date.getMonth() + 1).padStart(2, '0');
                const day = String(date.getDate()).padStart(2, '0');
                const hours = String(date.getHours()).padStart(2, '0');
                const minutes = String(date.getMinutes()).padStart(2, '0');
            
                const dateTimeDevice = `${year}-${month}-${day} ${hours}:${minutes}`;
                console.log(dateTimeDevice);
            
                // Update the UI
                updateDeviceDateTime(dateTimeDevice);        

			}
			else
			{
				let decoder = new TextDecoder('utf-8');
			}


		});
	} catch (error) {
		console.log('Error', error);
	}
}

document.getElementById('sendLogsOk').addEventListener('click', async () => {
	const CMD_LOGS_OK = 0x07; // Replace this with the actual command value if different

	// Construct the message as per the C code
	const message = new Uint8Array(27);

	message.set([0x23, 0x41, 0x52, 0x47, 0x23], 0); // "#ARG#"

	// Set additional bytes
	message.set([0x7E, 0x5E, 0x01, 0x00, 0x0B, 0xF3, 0xFF, 0x30, 0x00, 0x1C, 0xE5, 0x64, 0xA1, 0x43], 5);

	// Command and final upgrade ending message
	message[19] = CMD_LOGS_OK;
	message[20] = 0x01;

	// "#END#"
	message.set([0x23, 0x45, 0x4E, 0x44, 0x23], 21); // ASCII for "#END#"

	// Calculate CRC8-CCITT and add to message
	message[26] = crc8CCITT(0x00, message, 26);

	// Send the message via BLE
	try {
		await characteristic.writeValue(message);
		console.log('End upgrade message sent successfully');
	} catch (error) {
		console.error('Send error:', error);
	}
});




// CRC-8-CCITT Lookup Table (based on g_pucCrc8CCITT)
const crc8Table = [
	0x00, 0x07, 0x0E, 0x09, 0x1C, 0x1B, 0x12, 0x15,
	0x38, 0x3F, 0x36, 0x31, 0x24, 0x23, 0x2A, 0x2D,
	0x70, 0x77, 0x7E, 0x79, 0x6C, 0x6B, 0x62, 0x65,
	0x48, 0x4F, 0x46, 0x41, 0x54, 0x53, 0x5A, 0x5D,
	0xE0, 0xE7, 0xEE, 0xE9, 0xFC, 0xFB, 0xF2, 0xF5,
	0xD8, 0xDF, 0xD6, 0xD1, 0xC4, 0xC3, 0xCA, 0xCD,
	0x90, 0x97, 0x9E, 0x99, 0x8C, 0x8B, 0x82, 0x85,
	0xA8, 0xAF, 0xA6, 0xA1, 0xB4, 0xB3, 0xBA, 0xBD,
	0xC7, 0xC0, 0xC9, 0xCE, 0xDB, 0xDC, 0xD5, 0xD2,
	0xFF, 0xF8, 0xF1, 0xF6, 0xE3, 0xE4, 0xED, 0xEA,
	0xB7, 0xB0, 0xB9, 0xBE, 0xAB, 0xAC, 0xA5, 0xA2,
	0x8F, 0x88, 0x81, 0x86, 0x93, 0x94, 0x9D, 0x9A,
	0x27, 0x20, 0x29, 0x2E, 0x3B, 0x3C, 0x35, 0x32,
	0x1F, 0x18, 0x11, 0x16, 0x03, 0x04, 0x0D, 0x0A,
	0x57, 0x50, 0x59, 0x5E, 0x4B, 0x4C, 0x45, 0x42,
	0x6F, 0x68, 0x61, 0x66, 0x73, 0x74, 0x7D, 0x7A,
	0x89, 0x8E, 0x87, 0x80, 0x95, 0x92, 0x9B, 0x9C,
	0xB1, 0xB6, 0xBF, 0xB8, 0xAD, 0xAA, 0xA3, 0xA4,
	0xF9, 0xFE, 0xF7, 0xF0, 0xE5, 0xE2, 0xEB, 0xEC,
	0xC1, 0xC6, 0xCF, 0xC8, 0xDD, 0xDA, 0xD3, 0xD4,
	0x69, 0x6E, 0x67, 0x60, 0x75, 0x72, 0x7B, 0x7C,
	0x51, 0x56, 0x5F, 0x58, 0x4D, 0x4A, 0x43, 0x44,
	0x19, 0x1E, 0x17, 0x10, 0x05, 0x02, 0x0B, 0x0C,
	0x21, 0x26, 0x2F, 0x28, 0x3D, 0x3A, 0x33, 0x34,
	0x4E, 0x49, 0x40, 0x47, 0x52, 0x55, 0x5C, 0x5B,
	0x76, 0x71, 0x78, 0x7F, 0x6A, 0x6D, 0x64, 0x63,
	0x3E, 0x39, 0x30, 0x37, 0x22, 0x25, 0x2C, 0x2B,
	0x06, 0x01, 0x08, 0x0F, 0x1A, 0x1D, 0x14, 0x13,
	0xAE, 0xA9, 0xA0, 0xA7, 0xB2, 0xB5, 0xBC, 0xBB,
	0x96, 0x91, 0x98, 0x9F, 0x8A, 0x8D, 0x84, 0x83,
	0xDE, 0xD9, 0xD0, 0xD7, 0xC2, 0xC5, 0xCC, 0xCB,
	0xE6, 0xE1, 0xE8, 0xEF, 0xFA, 0xFD, 0xF4, 0xF3
];

// CRC8-CCITT calculation function using lookup table
function crc8CCITT(initialValue, data, length) {
	let crc = initialValue;
	console.log("crc8CCITT ", length);
	for (let i = 0; i < length; i++) {
		crc = crc8Table[(crc ^ data[i]) & 0xFF];
	}
	return crc;
}

$('#connect').on('click', function() {
    if (!isConnected) {
        connectToDevice();
    } else {
        disconnectFromDevice();
    }
});

function onDisconnected(event) {
    console.log('Device disconnected');
    isConnected = false;
    updateConnectButton();
    updateConnectionStatus(isConnected);

    // Disable buttons that require a connection
    $('#setConfig, #sleep, #getLogs, #sendLogsOk').prop('disabled', true);

    // Log the event
    addLog('Disconnected from the device.');
}

function disconnectFromDevice() {

    if (device && device.gatt.connected) {
        console.log('Disconnecting from device...');
        device.gatt.disconnect();
        // The 'gattserverdisconnected' event will handle the rest
    } else {
        console.log('Device is already disconnected.');
    }

    // Simulate successful disconnection
    isConnected = false;
    updateConnectButton();
    updateConnectionStatus(isConnected);
    // Disable other buttons
    $('#setConfig, #sleep, #getLogs, #sendLogsOk').prop('disabled', true);
    
    // Log the event
    addLog('Disconnected from the device.');
}

//document.getElementById('connect').addEventListener('click', async () => 
async function connectToDevice()
{
	try {
		console.log('Requesting Bluetooth Device...');
		device = await navigator.bluetooth.requestDevice({
			filters: [{ name: 'ARGOS' }],
			optionalServices: ['0ea24d33-bff6-4f3b-a59e-23f662931caa']
		});

		console.log('Connecting to GATT Server...');
		server = await device.gatt.connect();

		console.log('Getting Service...');
		const service = await server.getPrimaryService('0ea24d33-bff6-4f3b-a59e-23f662931caa');

		console.log('Getting Characteristic...');
		characteristic = await service.getCharacteristic('0ebf7e2f-b2ec-40dd-917d-dd8583179968');
/*
		//document.getElementById('sendPols').disabled = false;
		//document.getElementById('Open').disabled = false;
		//document.getElementById('Close').disabled = false;
		//	  document.getElementById('GetConfig').disabled = false;
		//document.getElementById('SetConfig').disabled = false;
		document.getElementById('SetEvt').disabled = false;
		//document.getElementById('FactorySettings').disabled = false;
		document.getElementById('sleep').disabled = false;
		document.getElementById('getLogs').disabled = false;
		document.getElementById('sendLogsOk').disabled = false;
		//document.getElementById('showPicker').disabled = false;
		document.getElementById('setDateTime').disabled = false;
		console.log('Connected and ready to interact!');
*/

		await enableNotifications();
		firstTime = 1;
		GetConfig();
		
        // Code to connect to the device...
        // On successful connection:

        isConnected = true;
        updateConnectButton();
        updateConnectionStatus(isConnected);
    
		setTimeout(function() {
			SetDateTime(); 
		}, 2000);
  
        // Enable buttons that require a connection
        $('#setConfig, #sleep, #getLogs, #sendLogsOk').prop('disabled', false);
        device.addEventListener('gattserverdisconnected', onDisconnected);
    
        // Log the event
        addLog('Connected to the device.');

	} catch (error) {
		console.log('Argh! ' + error);
	}
}

document.getElementById('clear').addEventListener('click', function() {
	//document.getElementById('messages').innerHTML = '';
	clearLogs();
});


document.getElementById('sleep').addEventListener('click', async () => {
	try {
		document.getElementById('inputText').value="s";
		const data = new TextEncoder().encode(document.getElementById('inputText').value);
		await characteristic.writeValue(data);
		console.log('Data sent to characteristic!');
	} catch (error) {
		alert("Sleeping!!");
	}
});


//document.getElementById('SetConfig').addEventListener('click', async () => {
async function SetConfig ()
{
	try {

		testCP = 1;

		zDevice.usDevice_CP = document.getElementById('inputCP').value;
		//console.log("Set config with cp : " + zDevice.usDevice_CP);
		const payload = createDeviceConfigPayload();
		// Send via BLE
		await characteristic.writeValue(payload);
		console.log('Device configuration sent!');

	} catch (error) {
		console.log('Send Error: ' + error);
	}
//});
}

//document.getElementById('SetEvt').addEventListener('click', async () => {
async function SetEvt ()
{
	try {

		testCP = 1;

		if ( document.getElementById('inputEvt').value == "1" )
		{
			zDevice.usEvent_Mask |= 0x0080; // unsigned short
			zDevice.usEvent_WakeUp |= 0x0080; // unsigned short
		} else  
		{
			zDevice.usEvent_Mask &= (~0x0080); // unsigned short
			zDevice.usEvent_WakeUp &= (~0x0080); // unsigned short
		}

		const payload = createDeviceConfigPayload();
		// Send via BLE
		await characteristic.writeValue(payload);
		console.log('Device configuration sent!');

	} catch (error) {
		console.log('Send Error: ' + error);
	}
//});
}

/*
document.getElementById('open').addEventListener('click', async () => {
	try {
		document.getElementById('inputText').value="p01";
		const data = new TextEncoder().encode(document.getElementById('inputText').value);
		await characteristic.writeValue(data);
		console.log('Data sent to characteristic!');
	} catch (error) {
		console.log('Send Error: ' + error);
	}
});


document.getElementById('close').addEventListener('click', async () => {
	try {
		document.getElementById('inputText').value="p00";
		const data = new TextEncoder().encode(document.getElementById('inputText').value);
		await characteristic.writeValue(data);
		console.log('Data sent to characteristic!');
	} catch (error) {
		console.log('Send Error: ' + error);
	}
});
*/
async function GetConfig()
{
	try {

		// Hex string data to send (31 bytes in hex)
		let hexString = "234152472341444553544f5035337542829a18040023454e44237a";

		// Function to convert a hex string to a byte array
		function hexToBytes(hex) {
			let bytes = [];
			for (let c = 0; c < hex.length; c += 2) {
				bytes.push(parseInt(hex.substr(c, 2), 16));
			}
			return new Uint8Array(bytes); // Return as Uint8Array for sending
		}

		// Convert the hex string to a byte array
		const data = hexToBytes(hexString);

		// Send the data
		await characteristic.writeValue(data);
		console.log('31 bytes of data sent!');
	} catch (error) {
		console.log('Send Error: ' + error);
	}

}


// Function to generate a timestamp for the filename
function getCurrentTimestamp() {
    const now = new Date();
    const year = now.getFullYear();
    const month = String(now.getMonth() + 1).padStart(2, '0'); // Months are zero-based
    const day = String(now.getDate()).padStart(2, '0');
    const hours = String(now.getHours()).padStart(2, '0');
    const minutes = String(now.getMinutes()).padStart(2, '0');
    const seconds = String(now.getSeconds()).padStart(2, '0');

    return `${year}${month}${day}_${hours}${minutes}${seconds}`;
}

// Function to handle the Save button click
$('#saveLogsButton').on('click', function() {
    if (isiOS()) {
        // iOS device detected
        // Execute the "Copy to Clipboard" functionality
        let logs = '';
        $('#logsContent p').each(function() {
            logs += $(this).text() + '\n';
        });

        // Copy to clipboard
        if (navigator.clipboard && window.isSecureContext) {
            // Use the Clipboard API
            navigator.clipboard.writeText(logs).then(function() {
                alert('Logs copied to clipboard');
            }, function(err) {
                console.error('Could not copy text: ', err);
                alert('Failed to copy logs to clipboard');
            });
        } else {
            // Fallback method for older browsers
            const textArea = document.createElement('textarea');
            textArea.value = logs;
            // Avoid scrolling to bottom
            textArea.style.top = '0';
            textArea.style.left = '0';
            textArea.style.position = 'fixed';
            document.body.appendChild(textArea);
            textArea.focus();
            textArea.select();

            try {
                const successful = document.execCommand('copy');
                const msg = successful ? 'Logs copied to clipboard' : 'Unable to copy logs';
                alert(msg);
            } catch (err) {
                console.error('Fallback: Could not copy text', err);
                alert('Failed to copy logs to clipboard');
            }

            document.body.removeChild(textArea);
        }
    } else {
        // Not an iOS device
        // Proceed with the original "Save Logs" functionality
        // Collect all the log messages
        let logs = '';
        $('#logsContent p').each(function() {
            logs += $(this).text() + '\n';
        });

        // Generate the filename with timestamp
        const timestamp = getCurrentTimestamp();
        const filename = `ArgosLogs_${timestamp}.txt`;

        // Create a Blob with the logs
        const blob = new Blob([logs], { type: 'text/plain' });

        // Create a link element
        const link = document.createElement('a');
        link.href = window.URL.createObjectURL(blob);
        link.download = filename;

        // Append the link to the body (required for Firefox)
        document.body.appendChild(link);

        // Trigger the download
        link.click();

        // Remove the link from the document
        document.body.removeChild(link);
    }
});

document.getElementById('getLogs').addEventListener('click', async () => {
	try {
		document.getElementById('inputText').value="l";
		getLogs = 1;
		const data = new TextEncoder().encode(document.getElementById('inputText').value);
		await characteristic.writeValue(data);
		console.log('GetLogs');
	} catch (error) {
		console.log('Send Error: ' + error);
	}
});

async function SetDateTime()
{
	const d = new Date();
	let epochTimeInSeconds = d.getTime()/1000;

	const byteArray = new Uint8Array([ 100,
		(epochTimeInSeconds & 0x000000ff),
		(epochTimeInSeconds & 0x0000ff00) >> 8,
		(epochTimeInSeconds & 0x00ff0000) >> 16,
		(epochTimeInSeconds & 0xff000000) >> 24
	]);

	console.log('Epoch time in seconds:', epochTimeInSeconds);
	console.log('Byte array:', byteArray);

	document.getElementById('dateTimePickerPopup').style.display = 'none';

	characteristic.writeValue(byteArray);
	console.log('Data sent to characteristic!');

}
/*
document.getElementById('setDateTime').addEventListener('click', () => {

	const d = new Date();
	let epochTimeInSeconds = d.getTime()/1000;

	const byteArray = new Uint8Array([ 100,
		(epochTimeInSeconds & 0x000000ff),
		(epochTimeInSeconds & 0x0000ff00) >> 8,
		(epochTimeInSeconds & 0x00ff0000) >> 16,
		(epochTimeInSeconds & 0xff000000) >> 24
	]);

	console.log('Epoch time in seconds:', epochTimeInSeconds);
	console.log('Byte array:', byteArray);

	document.getElementById('dateTimePickerPopup').style.display = 'none';

	characteristic.writeValue(byteArray);
	console.log('Data sent to characteristic!');

});
*/


