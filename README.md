# Example_APP

## Explicación del código del ejemplo

Este código es un ejemplo de como utilizar BLE desde un navegador conviertiendo este en una WebApp, este código puede utilizarse desde cualquier dispositivo (PC / Tablet / smart phone ). El único requisito es tener un browser y acceso a BLE y previeamente haber puesto este fichero en un servidor ( preferiblemente seguro ).

Desde un browser abrir este fichero.

Las acciones que se pueden hacer en el ejemplo :
- [ ] [Conectarse con los dispostivos ARGOS ( tiene el filtro para solo ver estos)]
- [ ] [Set Date&Time : permite poner la fecha y la hora ( popup para seleccionarla ), y visualiza la que tiene el equipo con otro popup ]
- [ ] [Fer Pols : hace un pulso de Bloqueo/Desbloqueo de la cerardura de la puerta, y escribe en la ventana de mensaje si la puerta está en posicion de cerrado o abierto]
- [ ] [Get Logs : pide todos los logs que haya en el equipo, los visualiza en el cuadro de text de abajo en formato HEX y en la venta mensaje escribe cuandos ha bajado ( en tiempo real van llegando )]

Para más información del código ejemplo mirar código: argoApp.html



